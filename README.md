# react2-cornerstone-viewport

> Cornerstone medical image viewport component for React

## Install

```bash
## NPM
npm install --save react2-cornerstone-viewport

## Yarn
yarn add react2-cornerstone-viewport
```

## Usage

```jsx
import React, { Component } from 'react';

import CornerstoneViewport from 'react2-cornerstone-viewport';

class Example extends Component {
  render() {
    return <CornerstoneViewport />;
  }
}
```
