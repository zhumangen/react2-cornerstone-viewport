import formatPN from './formatPN';
import formatPatId from './formatPatId';
import formatDA from './formatDA';
import formatTM from './formatTM';
import formatNumberPrecision from './formatNumberPrecision';
import isValidNumber from './isValidNumber';
import isSameOrientation from './isSameOrientation';
import formatAge from './formatAge';

const helpers = {
  formatPN,
  formatPatId,
  formatAge,
  formatDA,
  formatTM,
  formatNumberPrecision,
  isValidNumber,
  isSameOrientation,
};

export { helpers };
